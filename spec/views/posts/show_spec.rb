require 'spec_helper'

describe "Show post page" do

  subject { page }

  # title

  describe "should have post title as the page title" do
    let(:admin_user) { FactoryGirl.create(:admin_user) }
    let(:anonymous_post) { FactoryGirl.create(:anonymous_post, :user => nil) }

    before do
      sign_in admin_user
      visit post_path(anonymous_post)
    end

    it { should have_title(anonymous_post.title + " - dmtri") }
  end

  # admin user

  describe "Edit, PermaDelete, Delete links should be displayed for admin user for an anonymous post without user." do
    let(:admin_user) { FactoryGirl.create(:admin_user) }
    let(:anonymous_post) { FactoryGirl.create(:anonymous_post, :user => nil) }

    before do
      sign_in admin_user
      visit post_path(anonymous_post)
    end

    it { should have_link('PermaDelete', :href => post_path(anonymous_post)) }
    it { should have_link('Delete', :href => soft_destroy_post_path(anonymous_post)) }
    it { should have_link('Edit', :href => edit_post_path(anonymous_post)) }
  end

  describe "Edit, PermaDelete, Delete links should be displayed for admin user for an anonymous post with user." do
    let(:admin_user) { FactoryGirl.create(:admin_user) }
    let(:user) { FactoryGirl.create(:user) }
    let(:anonymous_post) { FactoryGirl.create(:anonymous_post, :user => user) }

    before do
      sign_in admin_user
      visit post_path(anonymous_post)
    end

    it { should have_link('PermaDelete', :href => post_path(anonymous_post)) }
    it { should have_link('Delete', :href => soft_destroy_post_path(anonymous_post)) }
    it { should have_link('Edit', :href => edit_post_path(anonymous_post)) }
  end

  describe "Edit, PermaDelete, Delete links should be displayed for admin user for a non-anonymous post with user." do
    let(:admin_user) { FactoryGirl.create(:admin_user) }
    let(:user) { FactoryGirl.create(:user) }
    let(:post) { FactoryGirl.create(:post, :user => user) }

    before do
      sign_in admin_user
      visit post_path(post)
    end

    it { should have_link('PermaDelete', :href => post_path(post)) }
    it { should have_link('Delete', :href => soft_destroy_post_path(post)) }
    it { should have_link('Edit', :href => edit_post_path(post)) }
  end

  describe "Edit, PermaDelete, Delete links should be displayed for admin user for an anonymous post made by the admin himself." do
    let(:admin_user) { FactoryGirl.create(:admin_user) }
    let(:post) { FactoryGirl.create(:anonymous_post, :user => admin_user) }

    before do
      sign_in admin_user
      visit post_path(post)
    end

    it { should have_link('PermaDelete', :href => post_path(post)) }
    it { should have_link('Delete', :href => soft_destroy_post_path(post)) }
    it { should have_link('Edit', :href => edit_post_path(post)) }
  end

    describe "Edit, PermaDelete, Delete links should be displayed for admin user for a non-anonymous post made by the admin himself." do
    let(:admin_user) { FactoryGirl.create(:admin_user) }
    let(:post) { FactoryGirl.create(:post, :user => admin_user) }

    before do
      sign_in admin_user
      visit post_path(post)
    end

    it { should have_link('PermaDelete', :href => post_path(post)) }
    it { should have_link('Delete', :href => soft_destroy_post_path(post)) }
    it { should have_link('Edit', :href => edit_post_path(post)) }
  end

  # regular user

  describe "No links should be displayed for regular user for an anonymous post without user." do
    let(:user) { FactoryGirl.create(:non_admin_user) }
    let(:anonymous_post) { FactoryGirl.create(:anonymous_post, :user => nil) }

    before do
      sign_in user
      visit post_path(anonymous_post)
    end

    it { should_not have_link('PermaDelete', :href => post_path(anonymous_post)) }
    it { should_not have_link('Delete', :href => soft_destroy_post_path(anonymous_post)) }
    it { should_not have_link('Edit', :href => edit_post_path(anonymous_post)) }
  end

  describe "No links should be displayed for regular user for an anonymous post with user." do
    let(:user) { FactoryGirl.create(:non_admin_user) }
    let(:user2) { FactoryGirl.create(:non_admin_user) }
    let(:anonymous_post) { FactoryGirl.create(:anonymous_post, :user => user2) }

    before do
      sign_in user
      visit post_path(anonymous_post)
    end

    it { should_not have_link('PermaDelete', :href => post_path(anonymous_post)) }
    it { should_not have_link('Delete', :href => soft_destroy_post_path(anonymous_post)) }
    it { should_not have_link('Edit', :href => edit_post_path(anonymous_post)) }
  end

  describe "No links should be displayed for regular user for a non-anonymous post with user." do
    let(:user) { FactoryGirl.create(:non_admin_user) }
    let(:user2) { FactoryGirl.create(:non_admin_user) }
    let(:post) { FactoryGirl.create(:post, :user => user2) }

    before do
      sign_in user
      visit post_path(post)
    end

    it { should_not have_link('PermaDelete', :href => post_path(post)) }
    it { should_not have_link('Delete', :href => soft_destroy_post_path(post)) }
    it { should_not have_link('Edit', :href => edit_post_path(post)) }
  end

  describe "No links should be displayed for regular user for an anonymous post made by admin." do
    let(:admin_user) { FactoryGirl.create(:admin_user) }
    let(:user) { FactoryGirl.create(:non_admin_user) }
    let(:post) { FactoryGirl.create(:anonymous_post, :user => admin_user) }

    before do
      sign_in user
      visit post_path(post)
    end

    it { should_not have_link('PermaDelete', :href => post_path(post)) }
    it { should_not have_link('Delete', :href => soft_destroy_post_path(post)) }
    it { should_not have_link('Edit', :href => edit_post_path(post)) }
  end

  describe "No links should be displayed for regular user for a non-anonymous post made by admin." do
    let(:admin_user) { FactoryGirl.create(:admin_user) }
    let(:user) { FactoryGirl.create(:non_admin_user) }
    let(:post) { FactoryGirl.create(:post, :user => admin_user) }

    before do
      sign_in user
      visit post_path(post)
    end

    it { should_not have_link('PermaDelete', :href => post_path(post)) }
    it { should_not have_link('Delete', :href => soft_destroy_post_path(post)) }
    it { should_not have_link('Edit', :href => edit_post_path(post)) }
  end

  describe "Edit and Delete links should be displayed for regular user for an anonymous post made by the regular user himself." do
    let(:user) { FactoryGirl.create(:non_admin_user) }
    let(:post) { FactoryGirl.create(:anonymous_post, :user => user) }

    before do
      sign_in user
      visit post_path(post)
    end

    it { should_not have_link('PermaDelete', :href => post_path(post)) }
    it { should have_link('Delete', :href => soft_destroy_post_path(post)) }
    it { should have_link('Edit', :href => edit_post_path(post)) }
  end

  describe "Edit and Delete links should be displayed for regular user for a non-anonymous post made by the regular user himself." do
    let(:user) { FactoryGirl.create(:non_admin_user) }
    let(:post) { FactoryGirl.create(:post, :user => user) }

    before do
      sign_in user
      visit post_path(post)
    end

    it { should_not have_link('PermaDelete', :href => post_path(post)) }
    it { should have_link('Delete', :href => soft_destroy_post_path(post)) }
    it { should have_link('Edit', :href => edit_post_path(post)) }
  end

  # non-user
  describe "User not signed in should not see any links for anonymous post without user" do
    let(:post) { FactoryGirl.create(:anonymous_post, :user => nil) }

    before do
      visit post_path(post)
    end

    it { should_not have_link('PermaDelete', :href => post_path(post)) }
    it { should_not have_link('Delete', :href => soft_destroy_post_path(post)) }
    it { should_not have_link('Edit', :href => edit_post_path(post)) }
  end

  describe "User not signed in should not see any links for anonymous post with user" do
    let(:user) { FactoryGirl.create(:non_admin_user) }
    let(:post) { FactoryGirl.create(:post, :user => user) }

    before do
      visit post_path(post)
    end

    it { should_not have_link('PermaDelete', :href => post_path(post)) }
    it { should_not have_link('Delete', :href => soft_destroy_post_path(post)) }
    it { should_not have_link('Edit', :href => edit_post_path(post)) }
  end

  describe "User not signed in should not see any links for non-anonymous post with user" do
    let(:user) { FactoryGirl.create(:non_admin_user) }
    let(:post) { FactoryGirl.create(:anonymous_post, :user => user) }

    before do
      visit post_path(post)
    end

    it { should_not have_link('PermaDelete', :href => post_path(post)) }
    it { should_not have_link('Delete', :href => soft_destroy_post_path(post)) }
    it { should_not have_link('Edit', :href => edit_post_path(post)) }
  end

  describe "User not signed in should not see any links for anonymous post by admin" do
    let(:user) { FactoryGirl.create(:admin_user) }
    let(:post) { FactoryGirl.create(:anonymous_post, :user => user) }

    before do
      visit post_path(post)
    end

    it { should_not have_link('PermaDelete', :href => post_path(post)) }
    it { should_not have_link('Delete', :href => soft_destroy_post_path(post)) }
    it { should_not have_link('Edit', :href => edit_post_path(post)) }
  end

  describe "User not signed in should not see any links for non-anonymous post by admin" do
    let(:user) { FactoryGirl.create(:admin_user) }
    let(:post) { FactoryGirl.create(:post, :user => user) }

    before do
      visit post_path(post)
    end

    it { should_not have_link('PermaDelete', :href => post_path(post)) }
    it { should_not have_link('Delete', :href => soft_destroy_post_path(post)) }
    it { should_not have_link('Edit', :href => edit_post_path(post)) }
  end
  
  describe "An anonymous user: reCaptcha sign up message" do
    let(:post) { FactoryGirl.create(:anonymous_post, :user => nil) }

    before do
      visit post_path(post)
    end

    it { should have_content "Sign up to avoid verification!" }
  end

  # After soft delete, undelete
  describe "After admin user soft deletes an anonymous post without user, he can undelete the post" do
    let(:user) { FactoryGirl.create(:admin_user) }
    let(:post) { FactoryGirl.create(:anonymous_post, :user => nil, :deleted => true) }

    before do
      sign_in user
      visit post_path(post)
    end

    it { should_not have_link('Delete', :href => soft_destroy_post_path(post)) }
    it { should have_link('Undelete', :href => revive_post_path(post)) }
  end

  describe "After admin user soft deletes an anonymous post with user, he can undelete the post" do
    let(:user) { FactoryGirl.create(:admin_user) }
    let(:non_admin_user) { FactoryGirl.create(:non_admin_user) }
    let(:post) { FactoryGirl.create(:anonymous_post, :user => non_admin_user, :deleted => true) }

    before do
      sign_in user
      visit post_path(post)
    end

    it { should_not have_link('Delete', :href => soft_destroy_post_path(post)) }
    it { should have_link('Undelete', :href => revive_post_path(post)) }
  end

  describe "After admin user soft deletes an anonymous post by himself, he can undelete the post" do
    let(:user) { FactoryGirl.create(:admin_user) }
    let(:post) { FactoryGirl.create(:anonymous_post, :user => user, :deleted => true) }

    before do
      sign_in user
      visit post_path(post)
    end

    it { should_not have_link('Delete', :href => soft_destroy_post_path(post)) }
    it { should have_link('Undelete', :href => revive_post_path(post)) }
  end

  describe "After admin user soft deletes a non-anonymous post, he can undelete the post" do
    let(:user) { FactoryGirl.create(:admin_user) }
    let(:non_admin_user) { FactoryGirl.create(:non_admin_user) }
    let(:post) { FactoryGirl.create(:post, :user => non_admin_user, :deleted => true) }

    before do
      sign_in user
      visit post_path(post)
    end

    it { should_not have_link('Delete', :href => soft_destroy_post_path(post)) }
    it { should have_link('Undelete', :href => revive_post_path(post)) }
  end

  describe "After admin user soft deletes a non-anonymous post by himself, he can undelete the post" do
    let(:user) { FactoryGirl.create(:admin_user) }
    let(:non_admin_user) { FactoryGirl.create(:non_admin_user) }
    let(:post) { FactoryGirl.create(:post, :user => non_admin_user, :deleted => true) }

    before do
      sign_in user
      visit post_path(post)
    end

    it { should_not have_link('Delete', :href => soft_destroy_post_path(post)) }
    it { should have_link('Undelete', :href => revive_post_path(post)) }
  end
  
  describe "Commenter names should link to user path" do
    let(:post) { FactoryGirl.create(:post) }
    let(:user) { FactoryGirl.create(:user) }
    
    before do
      Comment.create(:content => "something", :user_id => user.id, :post_id => post.id)
      visit post_path(post)
    end
    
    it do
      within(".comment-info") { should have_link(user.username, user_path(user)) }
    end
  end
end