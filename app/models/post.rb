class Post < ActiveRecord::Base
  attr_accessible :title, :content, :deleted, :anonymous
  attr_accessor :point, :verification, :urn

  paginates_per 20
  default_scope order('updated_at DESC')

  belongs_to :user
  has_many :comments, dependent: :destroy
  has_many :ratings

  validates :anonymous, inclusion: { in: [true, false] }
  validates :deleted, inclusion: { in: [true, false] }
  validates :title, length: { maximum: 300 }
  validate :title_and_content_should_not_be_nil_together, :title_and_content_should_not_be_empty_together

  def title_and_content_should_not_be_nil_together
    if !title && !content
        errors.add(:title, "or content should have something")
    end
  end

  def title_and_content_should_not_be_empty_together
    if title.blank?
      errors.add(:title, "cannot be empty.")
    elsif content.blank?
      errors.add(:content, "cannot be empty.")
    end
  end

  # eventually need a new way to calculate top posts
  def points
    ratings.count
  end

  def comment_number
    comments.count
  end

  def self.top_posts
    find(:all, :conditions => {deleted: false}).each do |post|
      post.point = (post.net_rating-1)/(((Time.now-post.created_at)/3600)**1.8)
    end
    .sort! { |a, b|  a.point <=> b.point }.reverse
  end

  def net_rating
    net = 0
    ratings.each do |rating|
      if rating.updown
        net += 1
      else
        net -= 1
      end
    end
    net
  end
  
  def set_urn
    self.urn = "posts/#{self.id}/#{self.title}".titler
  end
end