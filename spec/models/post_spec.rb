require 'spec_helper'

describe Post do

  let(:user) { FactoryGirl.create(:user) }
  before { @post = user.posts.build(title: "this is an example title.", content: "this is a fucking massive content!!", deleted: false, anonymous: true) }
  subject { @post }

  it { should respond_to(:title) }
  it { should respond_to(:content) }
  it { should respond_to(:deleted) }
  it { should respond_to(:anonymous) }
  it { should respond_to(:user_id) }
  its(:user) { should == user }

  it { should be_valid }

  describe "anonymous should not be nil" do
    before { @post.anonymous = nil }
    it { should_not be_valid }
  end

  describe "deleted should not be nil" do
    before { @post.deleted = nil }
    it { should_not be_valid }
  end

  describe "title and content should not be nil together" do
    before {
      @post.title = nil
      @post.content = nil
    }
    it { should_not be_valid }
  end

  describe "title should not be empty" do
    before { @post.title = "   " }
    it { should_not be_valid }
  end

  describe "content should not be empty" do
    before { @post.content = "   " }
    it { should_not be_valid }
  end

  describe "title should not be too long" do
    before { @post.title = "a" * 301}
    it { should_not be_valid }
  end
end