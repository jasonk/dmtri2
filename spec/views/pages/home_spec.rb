require 'spec_helper'

describe "Home page" do

  subject { page }

  describe "Posts in desktop view" do
    let(:user) { FactoryGirl.create(:user) }
    before do
      20.times { FactoryGirl.create(:post, :user => user) }
      visit root_path
    end
    after { Post.delete_all }

    # random = rand(20)+1
    #
    #     it { should have_link("this is an example title. #{random}", :href => post_path(random))}
  end

  describe "should have app name : moto as the title" do
    before { visit root_path }
    it { should have_title(app_title) }
  end

  describe "should display success message if user signs up" do
    before do
      sign_up("rammus", "leagueoflegends")
    end

    it { should have_css('.alert.alert-success', :text => 'Welcome! You have signed up successfully.') }
  end

  describe "should display success message if user signs out" do
    before do
      sign_up("rammus", "leagueoflegends")
      sign_out
    end

    it { should have_css('.alert.alert-success', :text => 'Signed out successfully.') }
  end

  describe "should display success message if user signs in" do
    user = User.new(:username => 'soraka', :password => 'pentakill')

    before do
      sign_up user.username, user.password
      sign_out
      sign_in user
    end

    it { should have_css('.alert.alert-success', :text => 'Signed in successfully.')}
  end
end
