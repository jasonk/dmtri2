require 'spec_helper'

describe PostsController do
  subject { page }

  let(:first_user_is_admin) { FactoryGirl.create(:admin_user) }

  describe "Not signed in user cannot see any kind of edit view for a post:" do

    describe "Post is anonymous without user_id" do
      let(:post) {FactoryGirl.create(:anonymous_post)}
      before do
        visit edit_post_path(post)
      end
      it { current_path.should == post_with_title_path(post, :title => post.title.titler) }
    end

    describe "Post is anonymous with user_id" do
      let(:post) { FactoryGirl.create(:anonymous_post, :user => first_user_is_admin) }
      before do
        visit edit_post_path(post)
      end
      it { current_path.should == post_with_title_path(post, :title => post.title.titler) }
    end

    describe "Post is not anonymous with user_id" do
      let(:post) { FactoryGirl.create(:post, :user => first_user_is_admin) }
      before do
        visit edit_post_path(post)
      end
      it { current_path.should == post_with_title_path(post, :title => post.title.titler) }
    end
  end

  describe "Signed in non-admin user cannot see edit view of someone else's post:" do

    let(:non_admin_user) { FactoryGirl.create(:non_admin_user) }
    before do
      sign_in non_admin_user
    end

    describe "Post is anonymous without user_id" do
      let(:post) {FactoryGirl.create(:anonymous_post)}
      before do
        visit edit_post_path(post)
      end
      it { current_path.should == post_with_title_path(post, :title => post.title.titler) }
    end

    describe "Post is anonymous with user_id" do
      let(:post) { FactoryGirl.create(:anonymous_post, :user => first_user_is_admin) }
      before do
        visit edit_post_path(post)
      end
      it { current_path.should == post_with_title_path(post, :title => post.title.titler) }
    end

    describe "Post is not anonymous with user_id" do
      let(:post) { FactoryGirl.create(:post, :user => first_user_is_admin) }
      before do
        visit edit_post_path(post)
      end
      it { current_path.should == post_with_title_path(post, :title => post.title.titler) }
    end
  end

  describe "Admin user see edit view of someone else's post:" do
    before do
      sign_in first_user_is_admin
    end

    describe "Post is anonymous without user_id" do
      let(:post) {FactoryGirl.create(:anonymous_post, :user => nil)}
      before do
        visit edit_post_path(post)
      end
      it { current_path.should == edit_post_path(post) }
    end

    describe "Post is anonymous with user_id" do
      let(:post) { FactoryGirl.create(:anonymous_post, :user => first_user_is_admin) }
      before do
        visit edit_post_path(post)
      end
      it { current_path.should == edit_post_path(post) }
    end

    describe "Post is not anonymous with user_id" do
      let(:post) { FactoryGirl.create(:post, :user => first_user_is_admin) }
      before do
        visit edit_post_path(post)
      end
      it { current_path.should == edit_post_path(post) }
    end
  end

end
