class ContactForm < ActionMailer::Base
  default from: "jason@dmtri.com"
  
  def question(email, content)
    @email = email
    @content = content
    
    mail(:to => "iamjsonkim@gmail.com", :subject => "Dmtri: question/suggestion")
  end
end
