require 'spec_helper'

describe "New post page" do

  subject { page }
  before { visit new_post_path }
  
  it { should have_content("New Post") }
  it { should have_css("label[for='post_title']") }
  it { should have_css("input#post_title") }
  it { should have_css("label[for='post_content']") }
  it { should have_css("textarea#post_content") }
  it { should have_css('#recaptcha') }
  it { should have_css("input[type='submit']")}
end

describe "When an anonymous user creates a post, the success message is shown" do

  subject { page }
  before do 
    visit new_post_path
    fill_in 'Title', :with => 'This is a title'
    fill_in 'Content', :with => "I've already found that when I want to set value to text field, text area or password field"
    click_button 'Submit'
  end
  
  it { should have_content "The post was created successfully."}
end

describe "When an anonymous user creates a post, the success message is shown" do
  let(:user) { FactoryGirl.create(:admin_user) }

  subject { page }
  before do 
    sign_in user
    visit new_post_path
    fill_in 'Title', :with => 'This is a title'
    fill_in 'Content', :with => "I've already found that when I want to set value to text field, text area or password field"
    click_button 'Submit'
  end
  
  it { should have_content "The post was created successfully."}
end

describe "When an anonymous user creates a post withot title, the error message is shown" do
  
  subject { page }
  before do
    visit new_post_path
    fill_in 'Content', :with => "I've already found that when I want to set value to text field, text area or password field"
    click_button 'Submit'
  end
  
  it { should have_content "title cannot be empty."}
end

describe "When an anonymous user creates a post withot title, the error message is shown" do
  let(:user) { FactoryGirl.create(:admin_user) }
  
  subject { page }
  before do
    sign_in user
    visit new_post_path
    fill_in 'Content', :with => "I've already found that when I want to set value to text field, text area or password field"
    click_button 'Submit'
  end
  
  it { should have_content "title cannot be empty."}
end

describe "An anonymous user: reCaptcha sign up message" do
  subject { page }
  before do
    visit new_post_path
  end
  
  it { should have_content "Sign up to avoid verification!" }
end