require 'spec_helper'

describe "User pages" do

  subject { page }

  describe "Sign in page" do
    before { visit new_user_session_path }
    it "should have the content 'Sign in'" do
      page.should have_content('Sign in')
    end

    it { should have_content("Username") }
    it { should have_content("Password") }
    it { should have_content("Remember me") }
    it { should have_button("Sign in") }
    it { should have_link("Sign up", href: new_user_registration_path) }
  end

  describe "Sign up page" do
    before { visit new_user_registration_path }
    it { should have_content("Sign up") }
    it { should have_content("Username") }
    it { should have_content("Password") }
    it { should have_content("Password confirmation") }
    it { should have_button("Sign up") }
    it { should have_link("Sign in", href: new_user_session_path) }
  end
end