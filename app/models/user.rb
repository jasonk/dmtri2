class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :rememberable, :trackable
         # :recoverable, :validatable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :password, :password_confirmation, :remember_me,
                  :username, :mod
                  # :email
  attr_protected :admin

  before_create :first_user_is_admin

  # Virtual attribute for authenticating by either username or email
  # This is in addition to a real persisted field like 'username'
  # attr_accessor :login
  # attr_accessible :login

  has_many :comments
  has_many :posts
  has_many :ratings

  validates :username, presence: true, length: { minimum: 4 }, uniqueness: { case_sensitive: false }
  validates :password, presence: true, length: { minimum: 6 }
  validates :password_confirmation, presence: true, length: { minimum: 6 }

  validate :username_should_not_be

  def username_should_not_be
    not_allowed = ["admin", "moderator", "mod", "administrator"]
    if not_allowed.include?(username.downcase)
      errors.add(:username, ", #{username} is not allowed.")
    end
  end

  # this is for loggin in with username and email
  # def self.find_first_by_auth_conditions(warden_conditions)
  #   conditions = warden_conditions.dup
  #   if login = conditions.delete(:login)
  #     where(conditions).where(["lower(username) = :value = :value", { :value => login.downcase }]).first
  #     # where(conditions).where(["lower(username) = :value OR lower(email) = :value", { :value => login.downcase }]).first
  #   else
  #     where(conditions).first
  #   end
  # end

  def upvote!(obj)
    if obj.is_a? Post
      upvote = ratings.create(updown: true, post_id: obj.id)
    elsif obj.is_a? Comment
      upvote = ratings.create(updown: true, comment_id: obj.id)
    else
      return false
    end
  end

  def downvote!(obj)
    if obj.is_a? Post
      upvote = ratings.create(updown: false, post_id: obj.id)
    elsif obj.is_a? Comment
      upvote = ratings.create(updown: false, comment_id: obj.id)
    else
      return false
    end
  end

  def total_points_for_posts
    points = 0
    posts.each do |post|
      points += post.net_rating
    end
    points
  end

  def total_points_for_comments
    points = 0
    comments.each do |comment|
      points += comment.net_rating
    end
    points
  end

  def total_points
    total_points_for_posts + total_points_for_comments
  end

  def posts_and_comments
    (posts + comments).sort! { |a, b|  b.created_at <=> a.created_at }
  end
  
  def non_anonymous_posts
    posts.select {|p| !p.anonymous }
  end
  
  def non_anonymous_comments
    comments.select {|c| !c.anonymous }
  end
  
  def non_anonymous_posts_and_comments
    (non_anonymous_posts + non_anonymous_comments).sort! { |a, b|  b.created_at <=> a.created_at }
  end
  
  def to_param
    username
  end

  private

    def first_user_is_admin
      if User.count < 1
        self.admin = true
      end
    end
end