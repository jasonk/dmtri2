def sign_in(user)
  visit new_user_session_path
  fill_in "Username", :with => user.username
  fill_in "Password", :with => user.password
  click_button "Sign in"
end

def sign_out
  within ("#sidebar") do
    click_link "Sign out"
  end
end

def sign_up(username, password)
  visit new_user_registration_path
  fill_in "Username", :with => username
  fill_in "user_password", :with => password
  fill_in "user_password_confirmation", :with => password
  click_button "Sign up"
end