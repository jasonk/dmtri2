FactoryGirl.define do
  factory :user do
    sequence(:username) {|n| "test#{n}"}
    password "qweqwe"
    password_confirmation "qweqwe"
    factory :non_admin_user do
      after(:create) do |non_admin_user|
        non_admin_user.admin = false
        non_admin_user.save
      end
    end
    factory :admin_user do
      after(:create) do |admin_user|
        admin_user.admin = true
        admin_user.save
      end
    end
  end

  factory :post do
    sequence(:title) {|n| "this is an example title. #{n}"}
    sequence(:content) {|n| "this is a fucking massive content!! #{n}"}
    user
    deleted false
    anonymous false
    factory :anonymous_post do
      anonymous true
    end
    factory :irregular_timed_post do
      after(:create) do |irregular_timed_post|
        irregular_timed_post.created_at -= (rand(12)+1)*3600
        irregular_timed_post.save
      end
    end
  end

  factory :comment do
    sequence(:content) {|n| "this is just a comment. #{n}"}
    user
    post
    deleted false
    anonymous false
    factory :anonymous_comment do
      anonymous true
    end
  end

  factory :rating do
    user
    post
    comment
    updown
  end
end