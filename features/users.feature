Feature: Users
  User registration

  Scenario: User can sign up
    Given user is signed out
    When  user is on "sign up" page
      And user enters "fake_username" as "user_username"
      And user enters "password" as "user_password"
      And user enters "password" as "user_password_confirmation"
    When  user submits "Sign up"
    Then  the page should have "Sign out"
      And the page should have "fake_username"

  Scenario: User can sign in
    # user signs up
    Given user is signed out
    When  user is on "sign up" page
      And user enters "fake_username" as "user_username"
      And user enters "password" as "user_password"
      And user enters "password" as "user_password_confirmation"
    When  user submits "Sign up"

    Given user is signed out
    When  user is on "sign in" page
      And user enters "fake_username" as "user_username"
      And user enters "password" as "user_password"
    When  user submits "Sign in"
    Then  the page should have "Sign out"
      And the page should have "fake_username"
