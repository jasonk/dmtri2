require 'spec_helper'

describe Comment do
  let(:user) { FactoryGirl.create(:user) }
  before { @comment = user.comments.build(content: "This is a content for a comment.") }

  subject { @comment }

  it { should respond_to(:content) }
  it { should respond_to(:deleted) }
  it { should respond_to(:anonymous) }
  it { should respond_to(:user_id) }
  it { should respond_to(:post_id) }
  it { should respond_to(:comment_id) }
  its(:user) { should == user }

  it { should be_valid }

  describe "anonymous should not be nil" do
    before { @comment.anonymous = nil }
    it { should_not be_valid }
  end

  describe "deleted should not be nil" do
    before { @comment.deleted = nil }
    it { should_not be_valid }
  end

  describe "content should not be nil" do
    before { @comment.content = nil }
    it { should_not be_valid }
  end

  describe "content should not be empty" do
    before { @comment.content = "    "}
    it { should_not be_valid }
  end

  describe "user_id should not be nil, if anonymous is false" do
    before {
      @comment.user_id = nil
      @comment.anonymous = false
    }
    it { should_not be_valid }
  end

end