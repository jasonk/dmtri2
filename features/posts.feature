Feature: Posts
  Registered users and anonymous users can interact with posts in a certain way.

  Scenario: Anonymous user can create a post
    Given user is on "new post" page
      And user enters "This is a freakin' title" as "post_title"
      And user enters "This must be the content" as "post_content"
    When  user submits "Create Post"
    Then  user is on "current post" page
      And the post "title" is "This is a freakin' title"
      And the post "content" is "This must be the content"

  Scenario: When user is not signed in, user is notified that he is posting anonymously
    Given user is signed out
    When  user is on "new post" page
    Then  the page should have "You are posting anonymously."
