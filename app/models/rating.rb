class Rating < ActiveRecord::Base
  attr_accessible :updown, :post_id, :comment_id, :user_id

  belongs_to :user
  belongs_to :post
  belongs_to :comment
end
