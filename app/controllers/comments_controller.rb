class CommentsController < ApplicationController
  before_filter :correct_user, only: [:edit, :update, :soft_destroy]
  before_filter :user_is_admin, only: :destroy

  def index
  end

  def show
    @post = Post.find(params[:post_id])
    @comment = Comment.find(params[:id])
  end

  def new
    @post = Post.find(params[:post_id])
    @comment = @post.comments.build
  end

  def create
    @post = Post.find(params[:post_id])

    if user_signed_in?
      comment_initialize

      if @comment.save
        @comment.ratings.create(:updown => true, :user_id => current_user.id) if user_signed_in?
        redirect_to @post
      else
        redirect_to @post
      end
    else
      if verify_recaptcha
        comment_initialize

        if @comment.save
          redirect_to @post
        else
          redirect_to @post
        end
      else
        redirect_to @post
      end
    end
  end

  def edit
    @post = Post.find(params[:post_id])
    @comment = @post.comments.find(params[:id])
  end

  def update
    @post = Post.find(params[:post_id])
    @comment = Comment.find(params[:id])
    if @comment.update_attributes(params[:comment])
      redirect_to [@post, @comment]
    else
      render "edit"
    end
  end

  def destroy
    @post = Post.find(params[:post_id])
    @comment = Comment.find(params[:id])
    @comment.destroy
    redirect_to @post
  end

  # other restful actions
  def reply
    @post = Post.find(params[:post_id])
    @comment = Comment.find(params[:id])
    @reply = @comment.comments.build
  end

  def create_reply
    @post = Post.find(params[:post_id])
    @parent = Comment.find(params[:id])
    @child = Comment.new(params[:comment])
    @child.comment_id = @parent.id
    @child.post_id = @post.id
    if user_signed_in?
      @child.user_id = current_user.id
      @child.anonymous = params[:comment][:anonymous]
    else
      @child.anonymous = true
    end

    if user_signed_in?
      if @child.save!
        redirect_to @post
      else
        render "reply"
      end
    else
      if verify_recaptcha
        if @child.save!
          redirect_to @post
        else
          render "reply"
        end
      else
        redirect_to reply_post_comment_path(@post, @parent)
      end
    end
  end

  def soft_destroy
    @post = Post.find(params[:post_id])
    @comment = Comment.find(params[:id])
    if @comment.update_attributes(deleted: true)
      redirect_to @post
    else
      render "edit"
    end
  end

  private

    def correct_user?(comment)
      user_signed_in? && (comment.user == current_user || current_user.admin)
      # comment.user || current_user == comment.user || !comment.anonymous
    end

    def user_is_mod?
      current_user.mod
    end

    def user_is_admin?
      current_user.admin
    end

    def correct_user
      post = Post.find(params[:post_id])
      comment = Comment.find(params[:id])
      redirect_to post_comment_path(post, comment) if !correct_user?(comment)
    end

    def user_is_admin
      comment = Comment.find(params[:id])
      redirect_to comment if !user_is_admin?
    end

    def comment_initialize
      @comment = nil
      if user_signed_in?
        @comment = current_user.comments.build(params[:comment])
      else
        @comment = Comment.new(params[:comment])
        @comment.anonymous = true
      end
      @comment.comment_id = params[:id]
      @comment.post_id = params[:post_id]
    end
end
