class CreateContactMails < ActiveRecord::Migration
  def change
    create_table :contact_mails do |t|
      t.string  :email
      t.text    :content
      t.timestamps
    end
  end
end
