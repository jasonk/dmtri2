10.times {|n| Post.create(:title => "Sense of loss #{n}", :content => "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.", :anonymous => false, :deleted => false)}

Post.all.each do |post|
  post.created_at -= 3600*(rand(12)+1)
  post.save
end

Post.all.each do |post|
  (rand(10)+1).times { post.ratings.create(:updown => true) }
end
