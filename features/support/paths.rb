def path_to(page_name)
  case page_name

  when /home/
    root_path
  when /sign up/
    new_user_registration_path
  when /sign in/
    new_user_session_path
  when /new post/
    new_post_path
  when /current post/
    post_path(Post.last)
  else
    if path = match_rails_path_for(page_name)
      path
    else
      raise "Can't find mapping from \"#{page_name}\" to a path.\n" +
      "Now, go and add a mapping in features/support/paths.rb"
    end
  end
end

def link_to(link_name)
  case link_name

  when /logo/
    "Dmtri"
  when /sign up/
    "Sign up"
  when /sign in/
    "Sign in"
  when /new post/
    "Submit a post anonymously"
  end
end

def match_rails_path_for(page_name)
  if page_name.match(/(.*) page/)
    return send "#{$1.gsub(" ", "_")}_path" rescue nil
  end
end
