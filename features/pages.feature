Feature: Pages
  mostly static pages

  Scenario: Logo links to home page
    Given user is on "home" page
    When  clicks on the "logo" in the right navigation
    Then  user should be on "home" page
    Given user is on "sign up" page
    When  clicks on the "logo" in the right navigation
    Then  user should be on "home" page
    Given user is on "sign in" page
    When  clicks on the "logo" in the right navigation
    Then  user should be on "home" page
    Given user is on "new post" page
    When  clicks on the "logo" in the right navigation
    Then  user should be on "home" page

  Scenario: Sign up link in the right navigation links to sign up page
    Given user is on "home" page
    When  clicks on the "sign up" in the right navigation
    Then  user should be on "sign up" page
    Given user is on "sign up" page
    When  clicks on the "sign up" in the right navigation
    Then  user should be on "sign up" page
    Given user is on "sign in" page
    When  clicks on the "sign up" in the right navigation
    Then  user should be on "sign up" page
    Given user is on "new post" page
    When  clicks on the "sign up" in the right navigation
    Then  user should be on "sign up" page

  Scenario: Sign in link in the right navigation links to sign in page
    Given user is on "home" page
    When  clicks on the "sign in" in the right navigation
    Then  user should be on "sign in" page
    Given user is on "sign up" page
    When  clicks on the "sign in" in the right navigation
    Then  user should be on "sign in" page
    Given user is on "sign in" page
    When  clicks on the "sign in" in the right navigation
    Then  user should be on "sign in" page
    Given user is on "new post" page
    When  clicks on the "sign in" in the right navigation
    Then  user should be on "sign in" page

  Scenario: New post link in the right navigation links to sign in page
    Given user is on "home" page
    When  clicks on the "new post" in the right navigation
    Then  user should be on "new post" page
    Given user is on "sign up" page
    When  clicks on the "new post" in the right navigation
    Then  user should be on "new post" page
    Given user is on "sign in" page
    When  clicks on the "new post" in the right navigation
    Then  user should be on "new post" page
    Given user is on "new post" page
    When  clicks on the "new post" in the right navigation
    Then  user should be on "new post" page