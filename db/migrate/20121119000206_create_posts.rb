class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.string  :title
      t.text    :content
      t.integer :user_id
      t.boolean :anonymous, default: false
      t.boolean :deleted, default: false
      t.timestamps
    end
  end
end