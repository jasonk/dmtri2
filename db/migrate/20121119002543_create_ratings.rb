class CreateRatings < ActiveRecord::Migration
  def change
    create_table :ratings do |t|
      t.boolean :updown
      t.integer :user_id
      t.integer :comment_id
      t.integer :post_id

      t.timestamps
    end

    add_index :ratings, :user_id
    add_index :ratings, :post_id
    add_index :ratings, :comment_id
    add_index :ratings, [:user_id, :post_id], unique: true
    add_index :ratings, [:user_id, :comment_id], unique: true
  end
end
