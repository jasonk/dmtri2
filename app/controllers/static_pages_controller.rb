class StaticPagesController < ApplicationController
  def about
  end
  
  def help
  end
  
  def contact
    @contact_mail = ContactMail.new
  end

  def mail_contact
    @contact_mail = ContactMail.new(params[:contact_mail])
    if verify_recaptcha && @contact_mail.save
      ContactForm.question(@contact_mail.email, @contact_mail.content).deliver
      flash[:notice] = "Email was delivered successfully."
      redirect_to root_path
    else
      flash[:error] = "Oops! Something went wrong. Please try again."
      render 'contact'
    end
  end
  
  def user_agreement
  end
  
  def privacy_policy
  end
end
