require 'spec_helper'

describe "Show comment page:" do

  subject { page }

  #admin user
  describe "Admin user should see PermaDelete, Delete, Edit and Reply links on userless anonymous comment" do
    let(:admin_user) { FactoryGirl.create(:admin_user) }
    let(:post) { FactoryGirl.create(:anonymous_post, :user => nil) }
    let(:comment) { FactoryGirl.create(:anonymous_comment, :user => nil, :post => post) }

    before do
      sign_in admin_user
      visit post_comment_path(post, comment)
    end

    it { should have_link('Reply', :href => reply_post_comment_path(post, comment)) }
    it { should have_link('PermaDelete', :href => post_comment_path(post, comment)) }
    it { should have_link('Delete', :href => soft_destroy_post_comment_path(post, comment)) }
    it { should have_link('Edit', :href => edit_post_comment_path(post, comment)) }
  end

  describe "Admin user should see PermaDelete, Delete, Edit and Reply links on anonymous comment with user" do
    let(:admin_user) { FactoryGirl.create(:admin_user) }
    let(:user) { FactoryGirl.create(:non_admin_user) }
    let(:post) { FactoryGirl.create(:anonymous_post, :user => user) }
    let(:comment) { FactoryGirl.create(:anonymous_comment, :user => user, :post => post) }

    before do
      sign_in admin_user
      visit post_comment_path(post, comment)
    end

    it { should have_link('Reply', :href => reply_post_comment_path(post, comment)) }
    it { should have_link('PermaDelete', :href => post_comment_path(post, comment)) }
    it { should have_link('Delete', :href => soft_destroy_post_comment_path(post, comment)) }
    it { should have_link('Edit', :href => edit_post_comment_path(post, comment)) }
  end

  describe "Admin user should see PermaDelete, Delete, Edit and Reply links on non-anonymous comment with user" do
    let(:admin_user) { FactoryGirl.create(:admin_user) }
    let(:user) { FactoryGirl.create(:non_admin_user) }
    let(:post) { FactoryGirl.create(:post, :user => user) }
    let(:comment) { FactoryGirl.create(:comment, :user => user, :post => post) }

    before do
      sign_in admin_user
      visit post_comment_path(post, comment)
    end

    it { should have_link('Reply', :href => reply_post_comment_path(post, comment)) }
    it { should have_link('PermaDelete', :href => post_comment_path(post, comment)) }
    it { should have_link('Delete', :href => soft_destroy_post_comment_path(post, comment)) }
    it { should have_link('Edit', :href => edit_post_comment_path(post, comment)) }
  end

  describe "Admin user should see PermaDelete, Delete, Edit and Reply links on anonymous comment with admin user himself" do
    let(:admin_user) { FactoryGirl.create(:admin_user) }
    let(:post) { FactoryGirl.create(:post, :user => admin_user) }
    let(:comment) { FactoryGirl.create(:anonymous_comment, :user => admin_user, :post => post) }

    before do
      sign_in admin_user
      visit post_comment_path(post, comment)
    end

    it { should have_link('Reply', :href => reply_post_comment_path(post, comment)) }
    it { should have_link('PermaDelete', :href => post_comment_path(post, comment)) }
    it { should have_link('Delete', :href => soft_destroy_post_comment_path(post, comment)) }
    it { should have_link('Edit', :href => edit_post_comment_path(post, comment)) }
  end

  describe "Admin user should see PermaDelete, Delete, Edit and Reply links on non-anonymous comment with admin user himself" do
    let(:admin_user) { FactoryGirl.create(:admin_user) }
    let(:post) { FactoryGirl.create(:post, :user => admin_user) }
    let(:comment) { FactoryGirl.create(:comment, :user => admin_user, :post => post) }

    before do
      sign_in admin_user
      visit post_comment_path(post, comment)
    end

    it { should have_link('Reply', :href => reply_post_comment_path(post, comment)) }
    it { should have_link('PermaDelete', :href => post_comment_path(post, comment)) }
    it { should have_link('Delete', :href => soft_destroy_post_comment_path(post, comment)) }
    it { should have_link('Edit', :href => edit_post_comment_path(post, comment)) }
  end

  #regular user
  describe "Regular user should see Reply link on userless anonymous comment" do
    let(:user) { FactoryGirl.create(:non_admin_user) }
    let(:post) { FactoryGirl.create(:anonymous_post, :user => nil) }
    let(:comment) { FactoryGirl.create(:anonymous_comment, :user => nil, :post => post) }

    before do
      sign_in user
      visit post_comment_path(post, comment)
    end

    it { should have_link('Reply', :href => reply_post_comment_path(post, comment)) }
    it { should_not have_link('PermaDelete', :href => post_comment_path(post, comment)) }
    it { should_not have_link('Delete', :href => soft_destroy_post_comment_path(post, comment)) }
    it { should_not have_link('Edit', :href => edit_post_comment_path(post, comment)) }
  end

  describe "Regular user should see Reply link on anonymous comment with user" do
    let(:user) { FactoryGirl.create(:non_admin_user) }
    let(:user2) { FactoryGirl.create(:non_admin_user) }
    let(:post) { FactoryGirl.create(:anonymous_post, :user => user2) }
    let(:comment) { FactoryGirl.create(:anonymous_comment, :user => user2, :post => post) }

    before do
      sign_in user
      visit post_comment_path(post, comment)
    end

    it { should have_link('Reply', :href => reply_post_comment_path(post, comment)) }
    it { should_not have_link('PermaDelete', :href => post_comment_path(post, comment)) }
    it { should_not have_link('Delete', :href => soft_destroy_post_comment_path(post, comment)) }
    it { should_not have_link('Edit', :href => edit_post_comment_path(post, comment)) }
  end

  describe "Regular user should see Reply link on non-anonymous comment with user" do
    let(:user) { FactoryGirl.create(:non_admin_user) }
    let(:user2) { FactoryGirl.create(:non_admin_user) }
    let(:post) { FactoryGirl.create(:post, :user => user2) }
    let(:comment) { FactoryGirl.create(:comment, :user => user2, :post => post) }

    before do
      sign_in user
      visit post_comment_path(post, comment)
    end

    it { should have_link('Reply', :href => reply_post_comment_path(post, comment)) }
    it { should_not have_link('PermaDelete', :href => post_comment_path(post, comment)) }
    it { should_not have_link('Delete', :href => soft_destroy_post_comment_path(post, comment)) }
    it { should_not have_link('Edit', :href => edit_post_comment_path(post, comment)) }
  end

  describe "Regular user should see Reply link on anonymous comment with admin user" do
    let(:user) { FactoryGirl.create(:non_admin_user) }
    let(:admin_user) { FactoryGirl.create(:admin_user) }
    let(:post) { FactoryGirl.create(:post, :user => admin_user) }
    let(:comment) { FactoryGirl.create(:anonymous_comment, :user => admin_user, :post => post) }

    before do
      sign_in user
      visit post_comment_path(post, comment)
    end

    it { should have_link('Reply', :href => reply_post_comment_path(post, comment)) }
    it { should_not have_link('PermaDelete', :href => post_comment_path(post, comment)) }
    it { should_not have_link('Delete', :href => soft_destroy_post_comment_path(post, comment)) }
    it { should_not have_link('Edit', :href => edit_post_comment_path(post, comment)) }
  end

  describe "Regular user should see Reply link on non-anonymous comment with admin user" do
    let(:user) { FactoryGirl.create(:non_admin_user) }
    let(:admin_user) { FactoryGirl.create(:admin_user) }
    let(:post) { FactoryGirl.create(:post, :user => admin_user) }
    let(:comment) { FactoryGirl.create(:comment, :user => admin_user, :post => post) }

    before do
      sign_in user
      visit post_comment_path(post, comment)
    end

    it { should have_link('Reply', :href => reply_post_comment_path(post, comment)) }
    it { should_not have_link('PermaDelete', :href => post_comment_path(post, comment)) }
    it { should_not have_link('Delete', :href => soft_destroy_post_comment_path(post, comment)) }
    it { should_not have_link('Edit', :href => edit_post_comment_path(post, comment)) }
  end

  describe "Regular user should see Delete, Edit link on anonymous comment with user himself" do
    let(:user) { FactoryGirl.create(:non_admin_user) }
    let(:post) { FactoryGirl.create(:post, :user => user) }
    let(:comment) { FactoryGirl.create(:anonymous_comment, :user => user, :post => post) }

    before do
      sign_in user
      visit post_comment_path(post, comment)
    end

    it { should have_link('Reply', :href => reply_post_comment_path(post, comment)) }
    it { should_not have_link('PermaDelete', :href => post_comment_path(post, comment)) }
    it { should have_link('Delete', :href => soft_destroy_post_comment_path(post, comment)) }
    it { should have_link('Edit', :href => edit_post_comment_path(post, comment)) }
  end

  describe "Regular user should see Delete, Edit link on non-anonymous comment with user himself" do
    let(:user) { FactoryGirl.create(:non_admin_user) }
    let(:post) { FactoryGirl.create(:post, :user => user) }
    let(:comment) { FactoryGirl.create(:comment, :user => user, :post => post) }

    before do
      sign_in user
      visit post_comment_path(post, comment)
    end

    it { should have_link('Reply', :href => reply_post_comment_path(post, comment)) }
    it { should_not have_link('PermaDelete', :href => post_comment_path(post, comment)) }
    it { should have_link('Delete', :href => soft_destroy_post_comment_path(post, comment)) }
    it { should have_link('Edit', :href => edit_post_comment_path(post, comment)) }
  end

  #anonymous user
  describe "Non-signed in user should see Reply on anonymous comment without user" do
    let(:post) { FactoryGirl.create(:anonymous_post, :user => nil) }
    let(:comment) { FactoryGirl.create(:anonymous_comment, :user => nil, :post => post) }

    before do
      visit post_comment_path(post, comment)
    end

    it { should have_link('Reply', :href => reply_post_comment_path(post, comment)) }
    it { should_not have_link('PermaDelete', :href => post_comment_path(post, comment)) }
    it { should_not have_link('Delete', :href => soft_destroy_post_comment_path(post, comment)) }
    it { should_not have_link('Edit', :href => edit_post_comment_path(post, comment)) }
  end

  describe "Non-signed in user should see Reply on anonymous comment with user" do
    let(:user) { FactoryGirl.create(:non_admin_user) }
    let(:post) { FactoryGirl.create(:anonymous_post, :user => user) }
    let(:comment) { FactoryGirl.create(:anonymous_comment, :user => user, :post => post) }

    before do
      visit post_comment_path(post, comment)
    end

    it { should have_link('Reply', :href => reply_post_comment_path(post, comment)) }
    it { should_not have_link('PermaDelete', :href => post_comment_path(post, comment)) }
    it { should_not have_link('Delete', :href => soft_destroy_post_comment_path(post, comment)) }
    it { should_not have_link('Edit', :href => edit_post_comment_path(post, comment)) }
  end

  describe "Non-signed in user should see Reply on non-anonymous comment with user" do
    let(:user) { FactoryGirl.create(:non_admin_user) }
    let(:post) { FactoryGirl.create(:post, :user => user) }
    let(:comment) { FactoryGirl.create(:comment, :user => user, :post => post) }

    before do
      visit post_comment_path(post, comment)
    end

    it { should have_link('Reply', :href => reply_post_comment_path(post, comment)) }
    it { should_not have_link('PermaDelete', :href => post_comment_path(post, comment)) }
    it { should_not have_link('Delete', :href => soft_destroy_post_comment_path(post, comment)) }
    it { should_not have_link('Edit', :href => edit_post_comment_path(post, comment)) }
  end

  describe "Non-signed in user should see Reply on anonymous comment with admin user" do
    let(:user) { FactoryGirl.create(:admin_user) }
    let(:post) { FactoryGirl.create(:post, :user => user) }
    let(:comment) { FactoryGirl.create(:anonymous_comment, :user => user, :post => post) }

    before do
      visit post_comment_path(post, comment)
    end

    it { should have_link('Reply', :href => reply_post_comment_path(post, comment)) }
    it { should_not have_link('PermaDelete', :href => post_comment_path(post, comment)) }
    it { should_not have_link('Delete', :href => soft_destroy_post_comment_path(post, comment)) }
    it { should_not have_link('Edit', :href => edit_post_comment_path(post, comment)) }
  end

  describe "Non-signed in user should see Reply on non-anonymous comment with admin user" do
    let(:user) { FactoryGirl.create(:admin_user) }
    let(:post) { FactoryGirl.create(:post, :user => user) }
    let(:comment) { FactoryGirl.create(:comment, :user => user, :post => post) }

    before do
      visit post_comment_path(post, comment)
    end

    it { should have_link('Reply', :href => reply_post_comment_path(post, comment)) }
    it { should_not have_link('PermaDelete', :href => post_comment_path(post, comment)) }
    it { should_not have_link('Delete', :href => soft_destroy_post_comment_path(post, comment)) }
    it { should_not have_link('Edit', :href => edit_post_comment_path(post, comment)) }
  end
  
  describe "Commenter names should link to user path" do
    let(:post) { FactoryGirl.create(:post) }
    let(:user) { FactoryGirl.create(:user) }
    let(:comment) { FactoryGirl.create(:comment, :user => user, :post => post) }
    
    before do
      visit post_comment_path(post, comment)
    end
    
    it do
      within(".comment-info") { should have_link(user.username, user_path(user)) }
    end
  end
end