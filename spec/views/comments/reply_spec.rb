require 'spec_helper'

describe "Reply comment page:" do

  subject { page }

  describe "A non-user can reply anonymously to a userless anonymous comment" do
    let(:post) { FactoryGirl.create(:anonymous_post, :user => nil) }
    let(:comment) { FactoryGirl.create(:anonymous_comment, :user => nil, :post => post) }

    before do
      visit reply_post_comment_path(post, comment)
      fill_in 'comment_content', :with => 'testing...'
      click_button 'Submit'
    end

    it { current_path.should == post_path(post) }
  end
  
  describe "An anonymous user: reCaptcha sign up message" do
    let(:post) { FactoryGirl.create(:anonymous_post, :user => nil) }
    let(:comment) { FactoryGirl.create(:anonymous_comment, :user => nil, :post => post) }
    
    before do
      visit reply_post_comment_path(post, comment)
    end
  
    it { should have_content "Sign up to avoid verification!" }
  end

  describe "A signedin-user can reply anonymously to a comment" do
    let(:post) { FactoryGirl.create(:anonymous_post, :user => nil) }
    let(:comment) { FactoryGirl.create(:anonymous_comment, :user => nil, :post => post) }
    let(:user) { FactoryGirl.create(:non_admin_user) }

    before do
      sign_in user
      visit reply_post_comment_path(post, comment)
      fill_in 'comment_content', :with => 'testing...'
      check('comment_anonymous')
      click_button 'Submit'
    end

    it { current_path.should == post_path(post) }
  end

  describe "signed in user can reply to a comment non anonymously." do
    let(:post) { FactoryGirl.create(:anonymous_post, :user => nil) }
    let(:user) { FactoryGirl.create(:non_admin_user) }
    let(:user2) { FactoryGirl.create(:non_admin_user) }
    let(:comment) { FactoryGirl.create(:comment, :user => user, :post => post) }

    before do
      sign_in user2
      visit reply_post_comment_path(post, comment)
      fill_in 'comment_content', :with => 'testing...'
      click_button 'Submit'
    end

    it { current_path.should == post_path(post) }
  end

  describe "admin user can reply to a comment anonymously" do
    let(:post) { FactoryGirl.create(:anonymous_post, :user => nil) }
    let(:admin) { FactoryGirl.create(:admin_user) }
    let(:user) { FactoryGirl.create(:non_admin_user) }
    let(:comment) { FactoryGirl.create(:comment, :user => user, :post => post) }

    before do
      sign_in admin
      visit reply_post_comment_path(post, comment)
      fill_in 'comment_content', :with => 'testing...'
      check('comment_anonymous')
      click_button 'Submit'
    end

    it { current_path.should == post_path(post) }
  end

  describe "admin user can reply to a comment non anonymously" do
    let(:post) { FactoryGirl.create(:anonymous_post, :user => nil) }
    let(:admin) { FactoryGirl.create(:admin_user) }
    let(:user) { FactoryGirl.create(:non_admin_user) }
    let(:comment) { FactoryGirl.create(:comment, :user => user, :post => post) }

    before do
      sign_in admin
      visit reply_post_comment_path(post, comment)
      fill_in 'comment_content', :with => 'testing...'
      click_button 'Submit'
    end

    it { current_path.should == post_path(post) }
  end
  
  describe "poster name should link to poster page" do
    let(:post) { FactoryGirl.create(:anonymous_post, :user => nil) }
    let(:user) { FactoryGirl.create(:non_admin_user) }
    let(:comment) { FactoryGirl.create(:comment, :user => user, :post => post) }
    
    before do
      visit reply_post_comment_path(post, comment)
    end
    
    it { within(".comment-info") { should have_link(user.username) } } 
  end
end