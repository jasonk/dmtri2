Given /^user is on "(.*?)" page$/ do |page_name|
  visit path_to(page_name)
end

# When /^clicks on the logo$/ do
#   within("#logo") do
#     click_link "Dmtri"
#   end
# end

When /^clicks on the "(.*?)" in the right navigation$/ do |link_name|
  within("#right") do
    click_link link_to(link_name)
  end
end

Then /^user should be on "(.*?)" page$/ do |page_name|
  current_path.should == path_to(page_name)
end

And /^user enters "(.*?)" as "(.*?)"$/ do |value, which_field|
  fill_in(which_field, with: value)
end

When /^user submits "(.*?)"$/ do |button|
  within(".submit") do
    click_on button
  end
end

And /^the post "(.*?)" is "(.*?)"$/ do |which_field, value|
  within("#"+which_field) do
    page.should have_content(value)
  end
end

Given /^user is signed out$/ do
  page.driver.submit :delete, destroy_user_session_path, {}
end

Then /^the page should have "(.*?)"$/ do |content|
  page.should have_content(content)
end