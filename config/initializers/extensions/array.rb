class Array
  def decreasing?
    for i in (0...self.size)
      return false if self[i] > self[i+1]
    end
    true
  end

  def increasing?
    for i in (0...self.size)
      return false if self[i] < self[i+1]
    end
    true
  end
end