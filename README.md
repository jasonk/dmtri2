dmtri
=====

intent
------
Best place for user anonymity and content centred website.

first phase
-----------

### Models and attributes
- user

    username

    email (optional)

    password

- post

    title

    content

    user_id (belongs to user)

    deleted

- comment

    content

    post_id (belongs to post)

    comment_id (belongs to comment)

    deleted

- rating

    updown :boolean

    user_id (belongs to user)

    post_id (belongs to post)

    or comment_id (belongs to comment)

### User stories

#### Undone

- captcha
- authorization for edit/delete posts

- User can change password
- User can close account
- User show view
- Fake voting view for non-signed in user
- report links and comments
- reported links and comments for admin
- user private message
- message model

#### Done


- post model

    title, content, user_id, deleted

- comment model

    content, post_id, comment_id, deleted

- rating model

    updown, user_id, post_id, comment_id

- devise user model

    for i in `find app/views/users -name '*.erb'` ; do html2haml -e $i ${i%erb}haml ; rm $i ; done

    https://github.com/plataformatec/devise/wiki/How-To:-Create-Haml-and-Slim-Views

- pages controller

    home page : at least routing

- How To: Allow users to sign in using their username or email addressNew Page Edit Page Page History

    https://github.com/plataformatec/devise/wiki/How-To:-Allow-users-to-sign-in-using-their-username-or-email-address

    user can sign in with username

    User only needs username, password and password verification to sign up

- Anyone can post

    anonymously: not as user

    anonymously: as user

    as user with username

- Anyone can comment

    anonymously: not as user

    anonymously: as user

    as user with username

- Tuesday: rating model for posts and comment 1

    rating model

    model attributes

    controller methods, upvote, downvote

    javascript render

    form for up and down in the view

    case differentiation for

- Only user with username can rate

- Post show view

- Post New view
- comment up down rating
- comment reply view

- User sign up view / custom sign up view?
- User sign in view

second phase
------------

- link
  - header
  - url
  - user_id (belongs to user)

- comment
  - link_id (belongs_to link)

- rating
  - link_id (belongs_to rating)

third phase
-----------

- photo
  - title
  - image_url
  or
  - uploaded
  - user_id (belongs to user)

- comment
  - photo_id (belongs_to link)

- rating
  - photo_id (belongs_to rating)

fourth phase
------------

- audio