class UsersController < ApplicationController
  def show
    @user = User.find_by_username(params[:id])
    @posts_and_comments = Kaminari.paginate_array(@user.posts_and_comments).page(params[:page]).per(20)
  end

  def destroy
    if current_user.admin == true
      @user = User.find_by_username(params[:id])
      @posts = @user.posts
      @comments = @user.comments

      @comments.destroy_all
      @posts.destroy_all
      @user.destroy

      flash[:success] = "The user was deleted."
      redirect_to root_path
    else
      redirect_to root_path
    end
  end

  def danger_zone
    @user = User.find_by_username(params[:id])
  end
end
