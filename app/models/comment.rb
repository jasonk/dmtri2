class Comment < ActiveRecord::Base
  attr_accessible :content, :deleted, :anonymous, :comment_id, :post_id, :user_id
  attr_accessor :verification

  belongs_to :user
  belongs_to :post
  belongs_to :comment
  has_many :comments
  has_many :ratings

  validates :anonymous, inclusion: { in: [true, false] }
  validates :deleted, inclusion: { in: [true, false] }
  validates :content, presence: true
  validate :user_id_should_not_be_nil_if_anonymous_is_false
  
  after_create :push_post_top

  def user_id_should_not_be_nil_if_anonymous_is_false
    if user_id == nil && !anonymous
      errors.add(:anonymous, "value wasn't assigned correctly.")
    end
  end

  def net_rating
    net = 0
    ratings.each do |rating|
      if rating.updown
        net += 1
      else
        net -= 1
      end
    end
    net
  end
  
  private
  
    def push_post_top
      self.post.update_attribute('updated_at', Time.now)
    end
end
