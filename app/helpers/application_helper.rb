module ApplicationHelper
  class CodeRayify < Redcarpet::Render::HTML
    def block_code(code, language)
      language ||= :plaintext
      CodeRay.scan(code, language).div
    end
  end
  
  def application_name
    "dmtri"
  end

  def application_description
    "share thoughts anonymously"
  end

  def app_title
    "#{application_name} : #{application_description}"
  end

  def title_maker(yielded_title)
    if yielded_title.size > 0
      (yielded_title + " - " + application_name).html_wrap("title")
    else
      app_title.html_wrap("title")
    end
  end
  
  def markdown
    coderayified = CodeRayify.new(:filter_html => true, 
                                :hard_wrap => true)
    
    Redcarpet::Markdown.new(coderayified, {
      :fenced_code_blocks => true,
      :lax_html_blocks => true,
      :no_intra_emphasis => true,
      :strikethrough => true,
      :underline => true,
      :highlight => true
    })
  end
end
