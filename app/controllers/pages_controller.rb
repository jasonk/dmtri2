class PagesController < ApplicationController
  def home
    @home = true
    @posts = Kaminari.paginate_array(Post.all).page(params[:page]).per(20)
  end
end
