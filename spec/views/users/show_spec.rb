require 'spec_helper'

describe "Show user page" do

  subject { page }

  # title

  describe "should have post title as the page title" do
    let(:admin_user) { FactoryGirl.create(:admin_user) }

    before do
      sign_in admin_user
      visit user_path(admin_user)
    end

    it { should have_title(admin_user.username + " page - dmtri") }
  end

  let(:user) { FactoryGirl.create(:user) }
  let(:non_admin_user) { FactoryGirl.create(:non_admin_user) }

  describe "Header includes right information and links" do
    before do
      10.times { FactoryGirl.create(:post, :user => user) }
      sign_in user
      visit user_path(user)
    end
    after { Post.delete_all }

    it { should have_css("#show-user-header", user.username) }
    it { should have_css("#show-user-header", user.total_points) }
    it { should have_css("#settings", "Settings") }
    it { should have_link("Change password") }
    it { should have_link("Delete account") }

    describe "Header should not include settings if the user is signed out" do
      before do
        sign_out
        visit user_path(user)
      end

      it { should_not have_css("#settings", "Settings")}
    end
  end

  describe "Content should display user posts" do
    before do
      10.times { FactoryGirl.create(:post, :user => user) }
      visit user_path(user)
    end
    after { Post.delete_all }

    it { should have_css(".post .entry .title") } #title
    it { should have_css(".post .entry .link_to_comments") } #comment
    it { should have_css(".post .entry .link_to_submitter") } #user
    it { should have_css(".post .entry span.submission_time") } #submitted time
  end

  describe "Content should display user comments" do
    let(:post) { FactoryGirl.create(:post, :user => user) }
    before do
      10.times { FactoryGirl.create(:comment, :user => user, :post => post)}
      visit user_path(user)
    end
    after { Comment.delete_all }

    it { should have_css(".comment .comment-body .comment-message") }

    # it { should have_css(".comment .comment-body .comment-message-below .comment-actions a", "show") }
    # it { should have_css(".comment .comment-body .comment-message-below .comment-actions a", "reply") }
  end

  describe "Content displays posts and comments in the order of latest to earliest" do

  end

  describe "Admin user should be signed and should be on his show user page to see right links for comments" do
    let(:user) { FactoryGirl.create(:user) }
    let(:post) { FactoryGirl.create(:post, :user => user) }
    before do
      10.times { FactoryGirl.create(:comment, :user => user, :post => post)}
      sign_in user
      visit user_path(user)
    end
    after { Comment.delete_all }

    it { should have_css(".comment .comment-body .comment-message-below .comment-actions a", "edit") }
    it { should have_css(".comment .comment-body .comment-message-below .comment-actions a", "delete") }
    it { should have_css(".comment .comment-body .comment-message-below .comment-actions a", "hard delete") }
    it { should have_css(".comment .comment-body .comment-message-below .comment-actions a", "permalink") }
  end

  describe "Nonadmin user should be signed and should be on his show user page to see right links for comments" do
    let(:non_admin_user) { FactoryGirl.create(:non_admin_user) }
    let(:post) { FactoryGirl.create(:post, :user => non_admin_user) }
    before do
      10.times { FactoryGirl.create(:comment, :user => non_admin_user, :post => post)}
      sign_in non_admin_user
      visit user_path(non_admin_user)
      non_admin_user.admin.should == false
    end
    after { Comment.delete_all }

    it { should have_css(".comment .comment-body .comment-message-below .comment-actions a", "edit") }
    it { should have_css(".comment .comment-body .comment-message-below .comment-actions a", "delete") }
    it { should_not have_css(".comment .comment-body .comment-message-below .comment-actions a.hard_delete", "hard delete") }
    it { should have_css(".comment .comment-body .comment-message-below .comment-actions a", "permalink") }
  end

  describe "Signed out user should only see permalink" do
    let(:post) { FactoryGirl.create(:post, :user => non_admin_user) }
    before do
      10.times { FactoryGirl.create(:comment, :user => non_admin_user, :post => post)}
      visit user_path(non_admin_user)
    end
    after { Comment.delete_all }

    it { should_not have_css(".comment .comment-body .comment-message-below .comment-actions a.edit_comment", "edit") }
    it { should_not have_css(".comment .comment-body .comment-message-below .comment-actions a.soft_delete", "delete") }
    it { should_not have_css(".comment .comment-body .comment-message-below .comment-actions a.hard_delete", "hard delete") }
    it { should have_css(".comment .comment-body .comment-message-below .comment-actions a", "permalink") }
  end
  
  describe "User link for comment" do
    let(:post) { FactoryGirl.create(:post) }
    let(:user) { FactoryGirl.create(:user) }
    
    before do
      Comment.create(:content => "something", :user_id => user.id, :post_id => post.id)
      visit user_path(user)
    end
    
    it do
      within(".comment-info") { should have_link(user.username, user_path(user)) }
    end
  end
end