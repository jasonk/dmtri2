class RatingsController < ApplicationController
  before_filter :authenticate_user!

  def post_upvote
    @post = Post.find(params[:rating][:post_id])
    current_user.upvote!(@post)
    respond_to do |format|
      format.html { redirect_to @post }
      format.js
    end
  end

  def post_downvote
    @post = Post.find(params[:rating][:post_id])
    current_user.downvote!(@post)
    respond_to do |format|
      format.html { redirect_to @post }
      format.js
    end
  end

  def comment_upvote
    @comment = Comment.find(params[:rating][:comment_id])
    current_user.upvote!(@comment)
    respond_to do |format|
      format.html { redirect_to @comment }
      format.js
    end
  end

  def comment_downvote
    @comment = Comment.find(params[:rating][:comment_id])
    current_user.downvote!(@comment)
    respond_to do |format|
      format.html { redirect_to @comment }
      format.js
    end
  end
end
