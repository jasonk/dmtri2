class PostsController < ApplicationController
  # before_filter :authenticate_user!, only: [:edit, :update, :destory]
  before_filter :correct_user, only: [:edit, :update, :soft_destroy, :revive]
  before_filter :user_is_admin, only: :destroy

  def index
    @posts = Post.all
  end

  def new
    if user_signed_in?
      @post = current_user.posts.build
    else
      @post = Post.new
    end
  end

  def create
    @post = Post.new
    if user_signed_in?
      post_initialize

      if @post.save
        @post.ratings.create(:updown => true, :user_id => current_user.id) if user_signed_in?
        flash[:success] = "The post was created successfully."
        redirect_to post_with_title_path(@post, :title => @post.title.titler)
      else
        render 'new'
      end
    else
      if verify_recaptcha
        post_initialize

        if @post.save
          flash[:success] = "The post was created successfully."
          redirect_to post_with_title_path(@post, :title => @post.title.titler)
        else
          render 'new'
        end
      else
        render 'new'
      end
    end
  end

  def show
    @post = Post.find(URI::decode(params[:id]))
    @post.set_urn
    @comment = @post.comments.build
    @comments = Comment.where("post_id = ? AND comment_id is NULL", @post.id)
  end

  def edit
    @post = Post.find(params[:id])
  end

  def update
    @post = Post.find(params[:id])
    if @post.update_attributes(params[:post])
      redirect_to @post
    else
      render 'edit'
    end
  end

  def destroy
    Post.find(params[:id]).destroy
    redirect_to root_path
  end

  def soft_destroy
    @post = Post.find(params[:id])
    if correct_user?(@post) || user_is_mod? || user_is_admin?
      if @post.update_attributes(deleted: true)
        flash[:success] = "The post was deleted successfully."
        redirect_to @post
      else
        flash[:error] = "Deleting post failed."
        redirect_to @post
      end
    else
      redirect_to @post
    end
  end

  def revive
    @post = Post.find(params[:id])
    if @post.update_attributes(deleted: false)
      flash[:success] = "The post was revived successfully."
      redirect_to @post
    else
      flash[:error] = "The post revive failed."
      redirect_to @post
    end
  end

  private

    def correct_user?(post)
      current_user && (post.user == current_user || current_user.admin)
    end

    def user_is_mod?
      current_user.mod
    end

    def user_is_admin?
      if current_user
        current_user.admin?
      else
        false
      end
    end

    def correct_user
      post = Post.find(params[:id])
      redirect_to post_with_title_path(post, :title => post.title.titler) if !correct_user?(post)
    end

    def user_is_admin
      post = Post.find(params[:id])
      redirect_to post_with_title_path(post, :title => post.title.titler) if !user_is_admin?
    end

    def post_initialize
      @post = nil
      if user_signed_in?
        @post = current_user.posts.build(params[:post])
      else
        @post = Post.new(params[:post])
        @post.anonymous = true
      end
    end
end
