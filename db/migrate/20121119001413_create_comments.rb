class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.text    :content
      t.integer :user_id
      t.integer :post_id
      t.integer :comment_id
      t.boolean :deleted, default: false
      t.boolean :anonymous, default: false
      t.timestamps
    end
  end
end
