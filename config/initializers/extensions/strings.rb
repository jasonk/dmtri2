class String
  def titler
    URI::encode(self.chomp("\.")[0..47].downcase)
  end

  def html_wrap(tag)
    "<#{tag}>#{self}</#{tag}>".html_safe
  end
end
