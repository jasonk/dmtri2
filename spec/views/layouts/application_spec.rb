require 'spec_helper'

describe "Application layout" do

  subject { page }

  describe "Sidebar in desktop view" do
    before { visit root_path }
    it { should have_link("Dmtri", :href => root_path)}
    it { should have_link("Sign up", :href => new_user_registration_path)}
    it { should have_link("Sign in", :href => new_user_session_path)}
    it { should have_link("Post", :href => new_post_path)}
  end

  describe "Sidebar in desktop view after user sign in" do
    let(:user) {FactoryGirl.create(:user)}
    before do
      sign_in user
      visit root_path
    end
    it { should have_link("Dmtri", :href => root_path)}
    it { should have_link(user.username, :href => user_path(user))}
    it { should have_link("Sign out", :href => destroy_user_session_path)}
    it { should have_link("Post", :href => new_post_path)}
  end
  
  describe "Head should have favicon" do
    it { has_selector?(:xpath, "//head/link[@href='favicon.ico']")}
  end
end