class ContactMail < ActiveRecord::Base
  attr_accessible :content, :email
  
  validates :email, :format => { :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i },
    :unless => Proc.new { |a| a.email.blank? }
end
